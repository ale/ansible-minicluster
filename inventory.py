#!/usr/bin/python
#
# Read a standard Ansible inventory and a 'service specification' file,
# and automatically assign service-specific roles to a subset of the
# available hosts.
#

import collections
import json
import optparse
import random
import yaml
from zlib import crc32

from ansible.parsing.dataloader import DataLoader
from ansible.vars import VariableManager
from ansible.inventory import Inventory


def predictable_random(*args):
    # Uses crc32(x) as seed.
    return random.Random(crc32(','.join(args)))


def load_static_inventory(path='hosts'):
    return Inventory(DataLoader(), VariableManager(), path)


def to_dynamic(static_inventory):
    dynamic_inventory = {'_meta': {'hostvars': {}}}
    for g in static_inventory.get_groups():
        if g == 'ungrouped':
            continue
        dynamic_inventory[g] = {}
        group = static_inventory.get_group(g)
        group_hosts = group.get_hosts()
        if group_hosts:
            dynamic_inventory[g]['hosts'] = map(str, group_hosts)
        group_vars = group.get_vars()
        if group_vars:
            dynamic_inventory[g]['vars'] = group_vars
        group_children = group.child_groups
        if group_children:
            dynamic_inventory[g]['children'] = map(str, group_children)
    for h in static_inventory.get_hosts():
        dynamic_inventory['_meta']['hostvars'][h.name] = dict(h.vars)
    return dynamic_inventory


def binpack(hosts, occupation_map, n):
    rnd = predictable_random(*hosts)
    result = []
    reverse_occupation_map = {}
    for h in hosts:
        reverse_occupation_map.setdefault(occupation_map[h], []).append(h)
    for v in sorted(reverse_occupation_map.keys()):
        tmp_hosts = reverse_occupation_map[v]
        rnd.shuffle(tmp_hosts)
        result.extend(tmp_hosts)
        if len(result) >= n:
            return result[:n]
    return result


# Assign services to hosts based on the num_instances, scheduling_group (which
# hosts) and the current occupation of the host.
#
# By default it schedules a service on the scheduling_group all with
# a num_instances of all (== all hosts of the scheduling_group).
#
# We are using binpack to equally distribute the load of services amongst the
# available hosts out of the scheduling group.
def assign_services(services_spec, inventory):
    hosts_by_service = {}
    host_occupation = collections.defaultdict(int)
    for service_name, service_spec in services_spec.iteritems():
        available_hosts = sorted(inventory[service_spec.get('scheduling_group', 'all')]['hosts'])
        num_instances = service_spec.get('num_instances', 'all')
        if num_instances == 'all':
            service_hosts = available_hosts
        else:
            service_hosts = binpack(available_hosts, host_occupation, num_instances)
        hosts_by_service[service_name] = service_hosts
        for h in service_hosts:
            host_occupation[h] += 1
    return hosts_by_service


def merge_inventory_with_assignments(services, service_assignments, inventory):
    # Set a default 'containers' variable for all hosts.
    for h in inventory['all']['hosts']:
        inventory['_meta']['hostvars'].setdefault(h, {}).setdefault('containers', {
            'enabled': [], 'disabled': [],
        })

    # Create custom groups for each service.
    for service, hosts in service_assignments.iteritems():
        inventory[service] = {'hosts': hosts}

        # Set enable_SERVICE for all hosts according to assignments.
        # Set the host_services variable for host->services mapping.
        service_flag = 'enable_' + service.replace('-', '_')
        for h in inventory['all']['hosts']:
            hostvars = inventory['_meta']['hostvars'].setdefault(h, {})
            hostvars[service_flag] = (h in hosts)
            hostvars.setdefault('host_services', []).append(services[service])

        # Set the container variables.
        for container in services[service].get('containers', []):
            for h in inventory['all']['hosts']:
                c = inventory['_meta']['hostvars'][h]['containers']
                data = {'service': services[service], 'container': container}
                if h in hosts:
                    c['enabled'].append(data)
                else:
                    c['disabled'].append(data)

    return inventory


# DEFAULT_SERVICE_CREDENTIALS = '''---
# - name: servicereg
#   enable_server: false
# - name: log-client
#   enable_server: false
# - name: root
#   enable_server: false
# '''

DEFAULT_SERVICES_YML = '''---
etcd:
  scheduling_group: core
  service_credentials:
    - name: etcd
      client_cert_mode: both
      extra_san:
        - "%host%._etcd-client-ssl._tcp.etcd"
  monitoring_endpoints:
    - job_name: etcd
      type: static
      port: 2379
skydns:
  scheduling_group: core
  service_credentials:
    - name: skydns
      enable_server: false
  monitoring_endpoints:
    - job_name: skydns
      type: static
      port: 5300
      scheme: http
nginx:
  scheduling_group: core
  service_credentials:
    - name: nginx
      enable_server: false
    - name: oidcproxy
  oauth2_clients:
    - id: oidcproxy
      name: OpenID-Connect Proxy
      redirect_endpoint: accounts
      grant_types:
        - authorization_code
        - client_credentials
      response_types:
        - code
      scopes:
        - hydra
log-collector:
  scheduling_group: core
  num_instances: 1
  service_credentials:
    - name: log-collector
      enable_client: false
  monitoring_endpoints:
    - job_name: mtail
      type: static
      port: 3903
      scheme: http
  public_endpoints:
    - name: logs
      type: static
      port: 3131
      scheme: http
      enable_oidcproxy: true
  oauth2_clients:
    - id: log-collector-oidc
      name: logviewer OIDC client
      redirect_endpoint: logs
      grant_types:
        - authorization_code
      response_types:
        - code
        - id_token
      scopes:
        - openid
prometheus:
  scheduling_group: core
  num_instances: 2
  service_credentials:
    - name: prometheus
      enable_client: true
      enable_server: true
  public_endpoints:
    - name: prometheus
      type: static
      port: 9090
      scheme: http
    - name: alertmanager
      type: static
      port: 9093
      scheme: http
hydra:
  num_instances: 1
  service_credentials:
    - name: hydra
      enable_client: false
  public_endpoints:
    - name: hydra
      type: static
      port: 4444
idp:
  num_instances: 1
  service_credentials:
    - name: idp
  public_endpoints:
    - name: accounts
      type: static
      port: 4443
  monitoring_endpoints:
    - job_name: idp
      type: static
      port: 4443
      scheme: https
  oauth2_clients:
    - id: consent-app
      name: Login Application
      redirect_endpoint: accounts
      grant_types:
        - authorization_code
        - client_credentials
      response_types:
        - code
        - id_token
      scopes:
        - hydra
        - openid
        - email
        - profile
    - id: idp-admin
      name: IDP Admin Client
      redirect_endpoint: accounts
      grant_types:
        - client_credentials
      response_types:
        - code
      scopes:
        - openid
        - idpadmin
  oauth2_policies:
    - description: Allow consent app to read keys
      subjects:
        - consent-app
      resources:
        - rn:hydra:keys:hydra.consent.response:private
        - rn:hydra:keys:hydra.consent.response:public
        - rn:hydra:keys:hydra.consent.challenge:public
      actions:
        - get
      effect: allow
    - description: Allow consent app to fetch client info
      subjects:
        - consent-app
      resources:
        - "rn:hydra:clients:<.*>"
      actions:
        - get
      effect: allow
    - description: Allow consent app to consult the Warden
      subjects:
        - consent-app
      resources:
        - rn:hydra:warden:token:allowed
      actions:
        - decide
      effect: allow
    - description: Allow admin API access
      subjects:
        - idp-admin
      resources:
        - admin.users
      actions:
        - read
        - write
      effect: allow
'''

def main():
    parser = optparse.OptionParser()
    parser.add_option('--list', action='store_true')
    parser.add_option('--host')
    opts, args = parser.parse_args()
    if args:
        parser.error('Too many arguments')

    if opts.list:
        # Load services.yml if present.
        services = yaml.safe_load(DEFAULT_SERVICES_YML)
        try:
            with open('services.yml', 'r') as fd:
                services.update(yaml.safe_load(fd))
        except (IOError, OSError):
            pass

        # In order to use 'services' as both a dict and a list, automatically
        # set the 'name' attribute on all elements.
        for service_name, service_spec in services.iteritems():
            service_spec.setdefault('name', service_name)
        
        # Load Ansible inventory.
        i = to_dynamic(load_static_inventory())

        # Assign services to hosts in inventory.
        service_assignments = assign_services(services, i)
        i = merge_inventory_with_assignments(services, service_assignments, i)

        # Inject service metadata into group_vars/all.
        i['all'].setdefault('vars', {})['services'] = services
        
        print json.dumps(i)


if __name__ == '__main__':
    main()
