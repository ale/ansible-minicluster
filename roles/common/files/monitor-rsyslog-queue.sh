#!/bin/sh

bytes_in_spool=$(ls -l /var/spool/rsyslog \
                        | awk '{sum+=$5} END {print sum}')

cat <<EOF | curl -s --data-binary @- http://localhost:9091/metrics/job/rsyslog/instance/$(hostname -s)
# TYPE rsyslog_queue_bytes counter
rsyslog_queue_bytes ${bytes_in_spool}
EOF

exit 0
