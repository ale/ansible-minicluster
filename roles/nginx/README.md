
The *nginx* role serves as the public frontend for services, operating
as a reverse proxy. It can be configured statically or dynamically
(see the documentation for the *public_endpoints* service metadata
attribute):

* the static configuration is just a list of upstream host:port pairs

* the dynamic configuration performs a SRV lookup to resolve a list of
  host:port pairs to use as upstreams. Unfortunately at the moment
  stock NGINX can't really do this very well (the paid version does):
  the DNS lookup is only performed once at startup time. It appears
  that id could be possible to implement this functionality with a
  pile of LUA (including maintaining a DNS cache, basically).

To simplify the public DNS setup, the *nginx* role only runs on the
hosts in the *core* group, though it is possible to override this if
needed.

Along with nginx, we run an instance
of [oidcproxy](https://git.autistici.org/ale/oidcproxy) to provide
OpenID-Connect authentication to services that only offer a
non-authenticated HTTP endpoint.

The oidcproxy serves HTTPS requests, regardless of the upstream, using
the same public certificates as NGINX. This isn't strictly necessary
given that we're talking to it over localhost, but it has been added
to allow NGINX to eventually failover to oidcproxy instances on other
nodes.

Every service handled by the oidcproxy needs its own OAuth2 client,
for the OpenID Connect workflow.
