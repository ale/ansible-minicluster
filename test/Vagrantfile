
API_VERSION = "2"

# Need to match the values set in group_vars/all.
DOMAIN = "minicluster-test.net"
DOMAIN_PUBLIC = "public.minicluster-test.net"

Vagrant.configure(API_VERSION) do |config|

  config.vm.box = "debian/stretch64"
  config.ssh.insert_key = false

  config.vm.provider :virtualbox do |vb, override|
    # Give VMs more memory.
    vb.customize ["modifyvm", :id, "--memory", "1024"]

    # Sync time every 3 seconds instead of the default of 20 minutes.
    vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 3000]
  end

  # Setup /etc/hosts.
  #
  # Hosts need to find each other using both the short name and the
  # fqdn. Furthermore, the OAuth workflow (even internally) uses the
  # public domain, so we also create an entry for that, pointing at a
  # random node (we assume that the public domain doesn't actually
  # exist).
  config.vm.provision "shell", inline: <<SCRIPT
cat > /etc/hosts <<EOF
127.0.0.1 localhost
192.168.10.10 node1.#{DOMAIN} node1
192.168.10.11 node2.#{DOMAIN} node2
192.168.10.12 node3.#{DOMAIN} node3
192.168.10.12 hydra.#{DOMAIN_PUBLIC}
EOF
SCRIPT

  config.vm.provision "fix-no-tty", type: "shell" do |s|
    s.privileged = false
    s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end

  config.vm.define "node1" do |m|
    m.vm.hostname = "node1"
    m.vm.network "private_network", ip: "192.168.10.10"
  end

  config.vm.define "node2" do |m|
    m.vm.hostname = "node2"
    m.vm.network "private_network", ip: "192.168.10.11"
  end

  config.vm.define "node3" do |m|
    m.vm.hostname = "node3"
    m.vm.network "private_network", ip: "192.168.10.12"
  end

end
