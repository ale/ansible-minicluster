
Test environment for ansible-minicluster. Turns up a 3-node cluster
with Vagrant, running the core services. The nodes will have "public"
IP addresses on the 192.168.10.0/24 network.

## Requirements

You'll need to install [x509ca](https://git.autistici.org/ale/x509ca)
to generate the service certificates. Assuming you have a
[Go](https://golang.org/) environment properly set up, run:

    $ go get git.autistici.org/ale/x509ca

This will install the *x509ca* binary in `$GOPATH/bin`. Make sure it
is included in your PATH.

Furthermore, it will be necessary to reach the Vagrant VMs over SSH on
their 192.168.10.x addresses. A `~/.ssh/config` snippet such as the
following might be useful:

    Host 192.168.10.*
        StrictHostKeyChecking no
        VerifyHostKeyDNS no
        UserKnownHostsFile /dev/null
        IdentitiesOnly yes
        IdentityFile ~/.vagrant.d/insecure_private_key

Finally, by default the test setup expects you to run a proxy for
Debian packages on your host node (on port 3142, the default for
*apt-cacher-ng*). This is highly recommended for testing, as it makes
setup a lot faster, but if you are **not** running a proxy you might
want to add `apt_proxy: noproxy` to the `group_vars/vagrant` file,
which is the file where you can overwrite variables for your vagrant
setup.
If you chose to run *apt-cacher-ng*, make sure it allows connections to
https endpoints by having the following line in your
`/etc/apt-cacher-ng/acng.conf`:

    PassThroughPattern .*:443

or something along those lines.

## Running tests

You're going to need Vagrant >= 1.9.5. To run the demo cluster:

    $ vagrant up
    $ ansible-playbook site.yml

## Known issues

### `node1: mesg: ttyname failed: Inappropriate ioctl for device`

This is an issue with Debian *stretch* having `mesg n` in
root's `.profile`. To solve it, upgrade your Vagrant installation
to a version greater or equal to 1.9.5.

## What's in there

At the moment, the test setup starts a basic cluster with the
fundamental infrastructure services, and it runs a test Docker
container with Redis, not actually used by any application, just to
show off the static service scheduler.

The cluster consists of three nodes in the 192.168.10.0/24 network,
without an internal network overlay. The domain name used for internal
services is *minicluster-test.net*, while the public (external) one is
*public.minicluster-test.net*.

Services can be reached via the HTTP routing layer (nginx), for
instance you can access the monitoring console at
https://prometheus.public.minicluster-test.net/ (assuming you've
tweaked your */etc/hosts* file to point the domain at one of the
Vagrant hosts).

The IDP currently has no default users, if you wish to create one it
can be done with a manual procedure:

* Find the host running the *idp* service (in the Ansible output, or
  simply by finding the host that has the *idp* package instaled)
* Run the *idp* admin command as root:

        $ idp create-user --name=user --email=foo@example.com \
		    --password=password --skip-confirmation

* Go to https://accounts.public.minicluster-test.net/ and log in.

